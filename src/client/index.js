const React = require('react');
const ReactDOM = require('react-dom');

const Interface = require('./components/Interface');

ReactDOM.render(
    <Interface />,
    document.getElementById('interface')
);
