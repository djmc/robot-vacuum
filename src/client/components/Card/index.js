const React = require('react');

const style = require('./style.scss');

/**
 * A simple card looking component.
 */
module.exports = function Card({children}) {

    return (
        <div className={style.box}>
            {children}
        </div>
    );

};
