const React = require('react');
const {shallow} = require('enzyme');

const Card = require('../index');

describe('client/components/Card', () => {
    it('should render', () => {
        const elem = shallow(<Card><h1>Hello</h1></Card>);
        expect(elem).toMatchSnapshot();
    });
});
