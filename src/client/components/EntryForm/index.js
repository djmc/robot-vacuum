const React = require('react');

const style = require('./style.scss');

/**
 * A component that renders a text area to input the instructions
 * for the robot, along with a helpful guide so you remember
 * the order the lines must be given in.
 */
class EntryForm extends React.Component {

    constructor(props) {
        super(props);

        // bind event handlers to this component
        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);

        this.state = {
            input: '',
            lineNumber: 0
        };
    }

    render() {

        let message;

        // provide some help depending on what lines have already
        // been typed in
        switch (this.state.lineNumber) {
            case 0:
            case 1:
                message = 'The grid size, x and y separated by a space.';
                break;
            case 2:
                message = 'The vacuum location, x and y separated by a space.';
                break;
            case 3:
                message = 'The location of a piece of dirt, x and y separated by a space.';
                break;
            default:
                message = 'The location of a piece of dirt, or the instructions for the robot.'
        }

        // adjust the height depending on how many lines have been typed up
        const inputHeight = this.state.lineNumber * 1.5;
        const inlineStyle = {
            height: `${inputHeight}em`
        };

        return(
            <form method="post" onSubmit={this.onSubmit}>

                <textarea name="vacuumInput"
                          className={style.textInput}
                          value={this.state.input}
                          onChange={this.onChange}
                          style={inlineStyle} />

                <div className={style.currentInstruction}>
                    {message}
                </div>

                <button className={style.button}>Send Instructions</button>

            </form>
        );
    }

    onChange(e) {
        // store the text in state, and keep a count of the number of lines
        const input = e.target.value;
        const lineNumber = input.split('\n').length;

        this.setState({
            input,
            lineNumber
        });
    }

    onSubmit(e) {
        e.preventDefault();
        // call the onFormSubmit function that has been passed in with the
        // instructions that have been provided
        this.props.onFormSubmit({
            inputInstructions: this.state.input
        });
    }

}

EntryForm.propTypes = {
    onFormSubmit: React.PropTypes.func.isRequired
};

module.exports = EntryForm;
