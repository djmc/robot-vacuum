const React = require('react');
const {shallow} = require('enzyme');

const EntryForm = require('../index');

describe('client/components/EntryForm', () => {
    it('should render', () => {
        const elem = shallow(<EntryForm onFormSubmit={() => {}} />);
        expect(elem).toMatchSnapshot();
    });
});
