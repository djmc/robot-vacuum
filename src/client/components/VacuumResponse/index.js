const React = require('react');

const style = require('./style.scss');

/**
 * Shows the response from the vacuum API endpoint. It simply shows
 * the final location, and the amount of dirt collection
 *
 * TODO: show a step-by-step animation of the robot moving around the grid
 */
function VacuumResponse({response}) {

    const {finalRobotLocation, dirtCollected} = response;
    return (
        <table className={style.table}>
            <tbody>
                <tr>
                    <td>Final location</td>
                    <td>{finalRobotLocation.join(', ')}</td>
                </tr>
                <tr>
                    <td>Dirt collected</td>
                    <td>{dirtCollected}</td>
                </tr>
            </tbody>
        </table>
    );

};

VacuumResponse.propTypes = {
    response: React.PropTypes.object.isRequired
};

module.exports = VacuumResponse;
