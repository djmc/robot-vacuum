const React = require('react');
const {shallow} = require('enzyme');

const VacuumResponse = require('../index');

const testResponse = {
    "dirtCollected":3,
    "finalRobotLocation":[4,4],
    "robotLocationHistory":[[2,2,false],[2,1,false],[1,1,true],[1,2,false],[1,3,false],[2,3,false],[3,3,true],[3,4,false],[4,4,true]],
    "dirtLocations":[[1,1],[3,3],[4,4]],
    "gridSize":[5,5],
    "commands":["S","W","N","N","E","E","N","E"]
};

describe('client/components/VacuumResponse', () => {

    it('should render', () => {

        const elem = shallow(<VacuumResponse response={testResponse} />);

        expect(elem).toMatchSnapshot();

    });

});
