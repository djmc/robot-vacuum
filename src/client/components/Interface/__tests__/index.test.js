const React = require('react');
const {shallow} = require('enzyme');

const Interface = require('../index');

describe('client/components/Interface', () => {
    it('should render', () => {
        const elem = shallow(<Interface />);
        expect(elem).toMatchSnapshot();
    });
});
