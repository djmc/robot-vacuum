const React = require('react');

const EntryForm = require('../EntryForm');
const VacuumResponse = require('../VacuumResponse');
const Card = require('../Card');

const style = require('./style.scss');

/**
 * The core of the app, it shows the various boxes of error messages, text entry
 * and the results of the vacuum commands. It also keeps all the state.
 *
 * As this is a simple app, I didn't set up Redux. However usually for a larger
 * React app, I would have set up Redux with Redux-Thunk.
 */
class Interface extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            currentResponse: null,
            lastError: null,
            isLoading: false
        };

        // bind event handlers to this component
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    render() {

        // if there is some response back from the server, render the
        // component that shows the response
        let responseInfo = null;
        if (this.state.currentResponse) {
            responseInfo = (
                <Card>
                    <VacuumResponse response={this.state.currentResponse} />
                </Card>
            );
        }

        // if there was an error message from the last request, then we want
        // to show it at the top of the page
        let errorMessage = null;
        if (this.state.lastError) {
            errorMessage = (
                <Card>
                    <h1 className={style.errorMessage}>{this.state.lastError}</h1>
                </Card>
            );
        }

        return (
            <div className={style.page}>
                { errorMessage }
                { responseInfo }
                <Card>
                    <EntryForm onFormSubmit={this.onFormSubmit} />
                </Card>
            </div>
        );
    }

    onFormSubmit({ inputInstructions }) {

        this.setState({
            lastError: null,
            isLoading: true
        });

        // use `fetch` to send the POST data
        return window.fetch('/api/vacuum/', {
                method: 'POST',
                body: inputInstructions
            })
            .then(response => {
                // parse the JSON but only if the status was OK
                if (response.ok) {
                    return response.json();
                } else {
                    // otherwise rethrow an error (will be caught later)
                    throw new Error(`Response was ${response.status}`);
                }
            })
            .then(json => {
                const {finalRobotLocation, dirtCollected} = json;

                // show the final location and amount of dirt collected in the
                // console, as was the requested minimum
                console.log(finalRobotLocation.join(' '));
                console.log(dirtCollected);

                // set the state with the response
                this.setState({
                    currentResponse: json,
                    isLoading: false
                });
            })
            .catch(err => {
                // if there was an error, put the message in the state
                this.setState({
                    lastError: err.message,
                    isLoading: false
                });
            })
    }

}

module.exports = Interface;
