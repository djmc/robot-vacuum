const express = require('express');
const bodyParser = require('body-parser');

const robotMiddleware = require('./robotMiddleware');

const app = express();

app.use('/static', express.static('static'));

// the vacuum API endpoint, parse the body text and call the robot middleware
app.post('/api/vacuum/', bodyParser.text(), robotMiddleware());

// render a simple shell when the root is requested
app.get('/', (req, res, next) => {

    res.send(`
        <!DOCTYPE html>
        <html>
            <head>
                <title>Robovacuum</title>
                <link rel="stylesheet" href="static/styles.css">
            </head>
            <body>
                <div id="interface"></div>
                <script src="static/main.js"></script>
            </body>
        </html>
    `);

    next();

});

// simple logger
app.use((req, res, next) => {
    console.log(`${req.method} ${res.statusCode} ${req.path}`);
    next();
});

app.listen(8080);
