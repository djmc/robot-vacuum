/**
 * The Grid is what keeps knowledge of where the robot is located,
 * where the dirt is and what dirt the robot has collected. Think
 * of it as an outside observer.
 */
class Grid {

    /**
     * Creates a Grid.
     * @param {Number} x - the width of the grid
     * @param {Number} y - the height of the grid
     * @param {Number[]} dirtLocations - the locations of dirt, as array pairs
     */
    constructor(x, y, dirtLocations = []) {
        this.x = x;
        this.y = y;
        this.dirtCleanedCount = 0;
        this.locationHistory = [];

        this.dirtGrid = {};

        // place any dirt passed in
        for (const [dirtX, dirtY] of dirtLocations) {
            this.addDirt(dirtX, dirtY);
        }
    }

    isInBounds(x, y) {
        return (x >= 0 && x < this.x && y >= 0 && y < this.y);
    }

    /**
     * Add dirt at the x, y locations on the grid.
     * @param {Number} x - the x location of the dirt
     * @param {Number} y - the y location of the dirt
     */
    addDirt(x, y) {
        if (!this.isInBounds(x, y)) {
            throw new Error(`Invalid dirt location, must be between 0-${this.x} and 0-${this.y}`);
        }
        this.dirtGrid[[x, y]] = true;
    }

    /**
     * At the x and y location, check if it is dirty, and clean it.
     *
     * @param {number} x - the x location to clean
     * @param {number} y - the y location to clean
     * @returns {boolean} true if the spot was cleaned, false otherwise
     */
    cleanLocation(x, y) {
        const isDirty = this.dirtGrid[[x, y]];

        if (isDirty) {
            this.dirtGrid[[x, y]] = false;
            return true;
        }
        return false;
    }

    /**
     * Adds a robot at the specified location. It will instantly clean that spot too.
     *
     * It will also add an onMove handler to the robot, so that we can keep track of its
     * movements.
     *
     * @param {Robot} robot - the robot to place
     * @param {number} x - the x location to place it
     * @param {number} y - the y location to place it
     */
    addRobot(robot, x, y) {
        this.robot = robot;

        if (!this.isInBounds(x, y)) {
            throw new Error(`Invalid robot location, must be between 0-${this.x} and 0-${this.y}`);
        }

        this.currentRobotLocation = [x, y];
        this.robot.onMove = this.onRobotMove.bind(this);
        this.onRobotMove();
    }

    /**
     * Handles when the robot moves. The robot will pass the x and y directions it
     * is moving in. It will then check if that location is cleaned, and push its
     * new location in the location history.
     *
     * It will check if it will go outside the bounds first, at which point, it will
     * just exit.
     *
     * @param {number} xDirection - the x direction that the robot is moving in
     * @param {number} yDirection - the y direction that the robot is moving in
     */
    onRobotMove({xDirection = 0, yDirection = 0} = {}) {
        let [currentX, currentY] = this.currentRobotLocation;

        currentX += xDirection;
        currentY += yDirection;

        if (!this.isInBounds(currentX, currentY)) {
            return;
        }

        const hasCleaned = this.cleanLocation(currentX, currentY);
        if (hasCleaned) {
            this.dirtCleanedCount++;
        }

        this.locationHistory.push([currentX, currentY, hasCleaned]);
        this.currentRobotLocation = [currentX, currentY];
    }

    /**
     * Returns the location history that was recorded while the robot is moving.
     * @returns {Array}
     */
    getLocationHistory() {
        return this.locationHistory;
    }

    /**
     * Gets the current robot location.
     * @returns {Array|*[]}
     */
    getCurrentRobotLocation() {
        return this.currentRobotLocation;
    }

    /**
     * Gets the number of dirt that was collected during the robots run.
     * @returns {number}
     */
    getCollectedDirtCount() {
        return this.dirtCleanedCount;
    }

}

module.exports = Grid;
