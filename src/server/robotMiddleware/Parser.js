module.exports = function parse(input) {

    // split the input by lines, then filter out the empty lines
    const lines = input.split('\n').filter(l => !!l);

    // basic data structure
    const data = {
        gridSize: null,
        robotLocation: null,
        dirtLocations: [],
        robotCommands: null
    };

    // we want to keep track of the index of the line we're processing
    for (let i = 0; i < lines.length; i++) {
        const line = lines[i];

        // robot commands are the last line, so everything else can be
        // processed first
        if (i < lines.length - 1) {

            // each of these are numbers separated by a space
            const coords = line.split(' ').map(Number);

            if (i === 0) {
                // first line is the grid size
                data.gridSize = coords;
            } else if (i === 1) {
                // second line is the robot location
                data.robotLocation = coords;
            } else {
                // remaining lines are dirt locations
                data.dirtLocations.push(coords);
            }
            continue;

        }

        // final line is the robot commands
        data.robotCommands = line;

    }

    return data;

};
