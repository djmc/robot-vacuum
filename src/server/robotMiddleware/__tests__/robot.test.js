const Robot = require('../Robot');

describe('server/robotMiddleware/Robot', () => {
    
    it('should error if onMove not set', () => {
        
        const robot = new Robot('N');
        expect(() => {
            robot.next()
        }).toThrow();
        
    });
    
    it('should error if no commands provided', () => {

        expect(() => {
            const robot = new Robot();
        }).toThrow();

    });

    describe('#next', () => {

        it('should return true when a command was run', () => {
            const robot = new Robot('N');
            robot.onMove = jest.fn();

            expect(robot.next()).toBe(true);
        });

        it('should return false when no command was run', () => {
            const robot = new Robot('N');
            robot.onMove = jest.fn();

            robot.next();
            expect(robot.next()).toBe(false);
        });

        it('should error when an invalid command is passed in', () => {
            const robot = new Robot('Z');
            robot.onMove = jest.fn();

            expect(() => {
                robot.next();
            }).toThrow();
        });

        it('should run on onMove handler when a command is run', () => {
            const robot = new Robot('N');
            const moveHandler = jest.fn();
            robot.onMove = moveHandler;

            robot.next();
            expect(moveHandler).toHaveBeenCalled();
        });

        [
            ['N', 0, 1],
            ['S', 0 , -1],
            ['E', 1, 0],
            ['W', -1, 0]
        ].forEach(([instruction, xDirection, yDirection]) => {
            it('should pass the direction it needs to move in to the move handler', () => {
                const robot = new Robot(instruction);
                const moveHandler = jest.fn();
                robot.onMove = moveHandler;

                robot.next();
                expect(moveHandler).lastCalledWith(
                    {
                        robot,
                        xDirection,
                        yDirection
                    }
                );
            });
        });

    });

    describe('#isDone', () => {

        it('should return false when there are still commands to run', () => {

            const robot = new Robot('NS');
            robot.onMove = jest.fn();

            robot.next();
            expect(robot.isDone()).toBe(false);

        });

        it('should return true when there are no more commands', () => {
            const robot = new Robot('NS');
            robot.onMove = jest.fn();

            robot.next();
            robot.next();
            expect(robot.isDone()).toBe(true);
        });

    });
    
});
