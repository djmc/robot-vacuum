const Grid = require('../Grid');

describe('server/robotMiddleware/Grid', () => {
    
    describe('#constructor', () => {

        it('should place dirt at locations', () => {
            const grid = new Grid(5, 5, [[1, 1], [2, 2]]);

            expect(grid.dirtGrid).toEqual({
                [[1, 1]]: true,
                [[2, 2]]: true
            });
        });

        it('should error at locations out of bounds', () => {

            expect(() => {
                new Grid(5, 5, [[6, 6]]);
            }).toThrow();

        });

    });

    describe('#addDirt', () => {

        it('should place dirt at the x, y coords specified', () => {

            const grid = new Grid(5, 5);

            grid.addDirt(1, 2);

            expect(grid.dirtGrid).toEqual({
                [[1, 2]]: true
            });

        });

        [
            [-1, -1],
            [6, 6],
            [-1, 0],
            [0, -1],
            [5, 5]
        ].forEach(([x, y]) => {

            it(`should error at out of bounds locations - ${x}, ${y}`, () => {
                const grid = new Grid(5, 5);
                expect(() => {
                    grid.addDirt(x, y);
                }).toThrow();
            });

        });

    });

    describe('#addRobot', () => {

        it('should place the robot at the location', () => {

            const grid = new Grid(5, 5);

            grid.addRobot({}, 2, 2);

            expect(grid.getCurrentRobotLocation()).toEqual([2, 2]);

        });

        [
            [-1, -1],
            [-1, 0],
            [0, -1],
            [5, 5],
            [6, 4],
            [4, 6]
        ].forEach(([x, y]) => {

            it(`should throw an error when placed out of bounds - ${x}, ${y}`, () => {
                const grid = new Grid(4, 4);
                expect(() => {
                    grid.addRobot({}, x, y);
                }).toThrow();
            });

        });

        it('should attach an onMove handler to the robot', () => {
            const grid = new Grid(5, 5);

            const robotMoveMock = jest.fn();
            grid.onRobotMove = robotMoveMock;

            const robot = {};
            grid.addRobot(robot, 2, 2);

            expect(robot.onMove).toBeDefined();
            robot.onMove();
            expect(robotMoveMock).toHaveBeenCalled();
        });

    });

    describe('#onRobotMove', () => {

        [
            [1, 0, [3, 2]],
            [-1, 0, [1, 2]],
            [0, -1, [2, 1]],
            [0, 1, [2, 3]]
        ].forEach(([xDirection, yDirection, expectedLocation]) => {

            it(`should move the robot in the direction it wants to go - ${xDirection}, ${yDirection}`, () => {

                const grid = new Grid(4, 4);

                grid.addRobot({}, 2, 2);

                grid.onRobotMove({xDirection, yDirection});

                expect(grid.getCurrentRobotLocation()).toEqual(expectedLocation);

            });

        });

        [
            [1, 0, [0, 0]],
            [0, 1, [0, 0]],
            [0, -1, [0, 0]],
            [-1, 0, [0, 0]]
        ].forEach(([xDirection, yDirection, expectedLocation]) => {

            it(`shouldn't move outside the bounds - ${xDirection}, ${yDirection}`, () => {

                const grid = new Grid(1, 1);
                grid.addRobot({}, 0, 0);
                grid.onRobotMove({xDirection, yDirection});

                expect(grid.getCurrentRobotLocation()).toEqual(expectedLocation);

            });

        });

        it('should clean the location that it moves to', () => {

            const grid = new Grid(4, 4, [[2, 2]]);
            grid.addRobot({}, 1, 1);
            grid.onRobotMove({
                xDirection: 1,
                yDirection: 0
            });
            grid.onRobotMove({
                xDirection: 0,
                yDirection: 1
            });

            expect(grid.getCurrentRobotLocation()).toEqual([2, 2]);
            expect(grid.getCollectedDirtCount()).toBe(1);

        });

        it('should not reclean the same dirt', () => {

            const grid = new Grid(4, 4, [[2, 1]]);
            grid.addRobot({}, 1, 1);
            grid.onRobotMove({
                xDirection: 1,
                yDirection: 0
            });
            // 2, 1

            grid.onRobotMove({
                xDirection: 1,
                yDirection: 0
            });
            // 3, 1

            grid.onRobotMove({
                xDirection: -1,
                yDirection: 0
            });
            // 2, 1

            grid.onRobotMove({
                xDirection: -1,
                yDirection: 0
            });
            // 1, 1

            expect(grid.getCollectedDirtCount()).toBe(1);
            expect(grid.dirtGrid).toEqual({
                [[2, 1]]: false
            });

        })

        it('should keep a history of where the robot has been', () => {

            const grid = new Grid(4, 4);
            grid.addRobot({}, 1, 1);
            grid.addDirt(2, 2);

            grid.onRobotMove({
                xDirection: 1,
                yDirection: 0
            });
            grid.onRobotMove({
                xDirection: 0,
                yDirection: 1
            });
            grid.onRobotMove({
                xDirection: -1,
                yDirection: 0
            });
            grid.onRobotMove({
                xDirection: 0,
                yDirection: -1
            });

            expect(grid.getLocationHistory()).toMatchSnapshot();
            expect(grid.getCurrentRobotLocation()).toEqual([1, 1]);
        });

    });
    
});
