const parser = require('../parser');

const INPUT_1 = `5 5
2 2
1 1
NSEW
`;

const INPUT_2 = `2 2
0 0
0 1
1 1
SSW
`;

describe('server/robotMiddleware/parser', () => {

    [
        INPUT_1,
        INPUT_2
    ].forEach((input, idx) => {

        it(`should parse the input - ${idx}`, () => {

            const data = parser(input);
            expect(data).toMatchSnapshot();

        });
        
    });

});
