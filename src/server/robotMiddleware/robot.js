// keep the x, y movements of each of the commands
const COMMANDS = {
    'N': [0, 1],
    'S': [0, -1],
    'E': [1, 0],
    'W': [-1, 0]
};

/**
 * The robot that moves. It is provided with a set of instructions, and will
 * report back to an onMove listener whenever it wants to move.
 */
class Robot {

    /**
     * Constructs the robot with the set of commands.
     * @param {string} commands - the commands to provide the robot with, made up
     *   with the letters N, S, E and W.
     */
    constructor(commands) {
        if (!commands) {
            throw new Error('No commands passed in');
        }
        this.commands = commands.split('');
    }

    /**
     * Run the next command, calling the onMove handler indicating the direction
     * that the robot is moving in. It will throw if it's an invalid command, or
     * if the onMove handler hasn't been attached.
     * @returns {boolean} true if a command was run, false if all out of commands
     */
    next() {

        if (!this.onMove) {
            throw new Error('No onMove handler set');
        }

        const command = this.commands.shift();

        if (!command) {
            return false;
        }

        // if this is not a valid command, then throw an error
        if (!COMMANDS[command]) {
            throw new Error(`Command input was invalid, must be one of ${Object.keys(COMMANDS).join(', ')}`);
        }

        // pass the x and y direction to the onMove handler that should be attached
        const [xDirection, yDirection] = COMMANDS[command];
        this.onMove({
            robot: this,
            xDirection,
            yDirection
        });

        return true;

    }

    /**
     * Will return True if no other commands are left to run.
     */
    isDone() {
        return (this.commands.length === 0);
    }

}

module.exports = Robot;
