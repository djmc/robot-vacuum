const parser = require('./parser');
const Robot = require('./Robot');
const Grid = require('./Grid');

/**
 * Runs the command input into a Grid and then returns the final result of
 * the state of the robot and grid.
 */
function runInput(input) {

    return new Promise(resolve => {

        const parsed = parser(input);

        const [gridX, gridY] = parsed.gridSize;
        const [robotX, robotY] = parsed.robotLocation;

        // create a grid, place the dirt and put the robot in the locations
        // that were input
        const grid = new Grid(gridX, gridY, parsed.dirtLocations);
        const robot = new Robot(parsed.robotCommands);
        grid.addRobot(robot, robotX, robotY);

        // keep looping over the robot commands until it's done
        while (!robot.isDone()) {
            robot.next();
        }

        resolve({
            dirtCollected: grid.getCollectedDirtCount(),
            finalRobotLocation: grid.getCurrentRobotLocation(),
            robotLocationHistory: grid.getLocationHistory(),
            dirtLocations: parsed.dirtLocations,
            gridSize: parsed.gridSize,
            commands: parsed.robotCommands.split('')
        });

    });

}

/**
 * Returns a middleware that processes the robot command input, and will
 * return a bunch of JSON of the final state.
 */
module.exports = function() {
    return (req, res, next) => {

        const {body} = req;

        return runInput(body)
            .then(postRunData => {

                // if it was successful, return a 200 with the data
                res.status(200).send(postRunData);
                next();

            })
            .catch(err => {
                // if an error occurred, then 500 out of here
                console.error(err);
                res.status(500).send({
                    error: err
                });
                next();

            });

    };
};
