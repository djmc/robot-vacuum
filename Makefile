.PHONY: build start test

build :
	docker-compose build

start :
	docker-compose up

test :
	docker-compose run web npm run test -s
