# Tray.io Frontend Tech Test

:wave: Hi,

This is my attempt at the tech test that you have outlined.

### Get going

You need Docker and Docker Compose installed and set up.

1. Clone the repository,
2. `make build`
3. `make start`
4. Go to http://localhost:8080/

### Running unit tests

There is a set of unit tests, they can be run with `make test`.

### Tech

This is the list of tech that I used to build this.

#### Docker

I can be sure it'll run on your machine with Docker. You can run without Docker
as well if you so wish (using `npm install` and `npm run start`).

#### Node, Express

I'm quite adept at Javascript these days that it made sense to just go with what
I know. Express.js was used as the server as I find it quite straight-forward to 
set up, compared to Koa.js which is very expandable but a lot of moving parts
(for example, Express.js comes with routing and static file serving out of the
box, but Koa.js does not).

#### Jest

I use Jest as the testing framework as it's very easy to set up. I can just install
it and start writing tests. The assertions and mocking frameworks as well are quite
broad, and snapshotting is very helpful to quickly build tests that ensure they
produce the correct output.

#### React

I feel like the use for this task was a bit unjustified. However I ended up using
it so that I can get a nice and dynamic user interface running.

Unfortunately, due to time constraints, I didn't get around to building a Redux
state layer. So the entire state, which is fortunately quite simple, is kept
in a single component - `Interface`. Preferably, I would have used Redux and
Redux-Thunk's to get this running properly. It would have come with the added
bonus of isomorphism.

#### CSS Modules

I've only recently discovered these, and they're great for having locally scoped
CSS for components. You don't have to worry about clashing names, and when you're
done using a certain style, you just delete it without worrying if some other
component is using it.
